import { Card, CardImg } from "reactstrap";
import {images} from "../assets/img/images";
export function Tarjeta({ datos }) { 
  return (
    <Card className="col-3 pt-4 px-2 mx-5 my-5" style={{ display: "flex", borderRadius:'20px'}}>
      {datos?.map((integrante) => (
        <div className="d-flex">
          <div className="mb-2" style={{width:'90px'}}>
            <CardImg
              style={{ objectFit: "cover", width: "100px", height: "100px" }}
              className="rounded-circle border-0"
              alt="Card image cap"
              src={images[integrante?.Foto]}
            />
          </div>
          <div className="px-5 py-4 ">
            <div>
              <h5>{integrante?.Nombres}</h5>
              <h6>{integrante?.Cargo}</h6>
            </div>
          </div>
        </div>
      ))}
    </Card>
  );
}
