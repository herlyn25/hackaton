import { hacerGrupos } from "../utils/funciones";

export function NumeroIntegrantes({ valor, setValor, setData }) {  
  return (
    <div className="mb-3 col-5">
      <input
        type="number"
        className="form-control"
        placeholder="Numero de integrantes"
        value={valor}
        min="1"
        max="10"
        onChange={(evento) => { 
          setValor(evento.target.value)
          if(localStorage.getItem("arreglo")===""){
            alert("Cargue un archivo")
          }else{
             let datos = JSON.parse(localStorage.getItem("arreglo"));             
             setData(hacerGrupos(datos, evento.target.value));   
          }                
        }}
      />
    </div>
  );
}
