import * as XLSX from "xlsx";
import { hacerGrupos } from "../utils/funciones";

export function ObtenerArchivo({ setData, cantidad_participantes }) {       
    const onchange = (event) => {    
    const [file] = event.target.files;
    const extension = event.target.files[0].name.split(".")[1];
    const reader = new FileReader();
    if(extension==="xlsx" || extension==="xls"){
      reader.onload = (evt) => {
          const bstr = evt.target.result;      
          const wb = XLSX.read(bstr, { type: "binary" });
          const wsname = wb.SheetNames[0];
          const ws = wb.Sheets[wsname];
          const datos = XLSX.utils.sheet_to_json(ws, {});
          localStorage.setItem("arreglo", JSON.stringify(datos)); 
          setData(hacerGrupos(datos, cantidad_participantes));      
        };   
        reader.readAsBinaryString(file); 
    }else if(extension==="json"){  
        const fileReader = new FileReader();
        fileReader.readAsText(event.target.files[0], "UTF-8");
        fileReader.onload = (e) => {
        localStorage.setItem("arreglo", e.target.result); 
        let json = JSON.parse(e.target.result);
        setData(hacerGrupos(json,cantidad_participantes))        
        }
       
    }else{
      alert("Solo se permiten extensiones xlsx, xls y JSON");
    }  
  };  
  return (
    <div>
      <input type="file" accept=".xlsx, .xls,.json" onChange={onchange}/>
    </div>
  );
}