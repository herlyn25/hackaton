import { Routes, Route, Link } from "react-router-dom";

export function NavBar() {
    return(
        <nav className="navbar navbar-expand-lg navbar navbar-dark bg-primary">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/">Hackaton</Link>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mb-2" style={{marginLeft:'88%'}}>
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link active" to={"/generator"}>Generator</Link>
                        </li>                      
                    </ul>
                </div>
            </div>
        </nav>
    );
}