import logo from '../../assets/picture/lsv.png'
const footerStyle ={
    width:'100vw',
    height:'17em',
    position:'absolute'
}
const logoStyle ={
    width:'200px',
    height:'150px'
}

export function Footer() {

    return(
        <div className="bg-primary" style={footerStyle}>
            <div className="cont">
                <div className="name">
                    <h3 className="h3footer">Hackaton - CAR IV</h3>
                    <img style={logoStyle} src={logo} alt="logo" />
                </div>
                <div className="devteam">
                    <p className='devtittle'>Desarrollado por:</p>
                    <p className='dev' >Herly de J. Castillo</p>
                    <p className='dev' >Maria Anaya</p>
                    <p className='dev' >Jhonatan Alvarez</p>
                </div>
                <div className="icons">
                    <p className='redes'>Redes</p>
                    <i className="fab fa-facebook-square icons__i "></i>
                    <i className="fab fa-instagram-square  icons__i"></i>
                    <i className="fab fa-twitter-square  icons__i"></i>
                </div>
            </div>
            
            <div className="copyRigth">
                <p style={{marginTop: '-20px'}}>
                    &copy; Derechos Reservados - 2022
                </p>
            </div>
        </div>
    );
}