import alansanchez from "./alan-sanchez.jpg";
import alvarovergara from "./alvaro-vergara.jpg";
import amandaquintana from "./amanda-quintana.jpg";
import andresgarcia from "./andres-garcia.jpg";
import andresmolinares from "./andres-molinares.jpg";
import angelicamorales from "./angelica-morales.jpg";
import danithbabilonia from "./danith-babilonia.jpg";
import deivervazquez from "./deiver-vazquez.jpg";
import erickcontreras from "./erick-contreras.jpg";
import erickherazo from "./erick-herazo.jpg";
import evernavarro from "./ever-navarro.jpg";
import herlycastillo from "./herly-castillo.jpg";
import isaacbenavides from "./isaac-benavides.jpg";
import jaircalderon from "./jair-calderon.jpg";
import jhonatanalvarez from "./jhonatan-alvarez.jpg";
import jorgezetien from "./jorge-zetien.jpg";
import juanlara from "./juan-lara.jpg";
import juanpayares from "./juan-payares.jpg";
import juansierra from "./juan-sierra.jpg";
import leandromeza from "./leandro-meza.jpg";
import luispayares from "./luis-payares.jpg";
import luistilve from "./luis-tilve.jpg";
import mariaanaya from "./maria-anaya.jpg";
import mauricioarias from "./mauricio-ariaspng.jpg";
import oscarballestas from "./oscar-ballestas.jpg";
import ricardojaramillo from "./ricardo-jaramillo.jpg";
import ronaldpaternina from "./ronald-paternina.jpg";
import steevenaltamiranda from "./steeven-altamiranda.jpg";
import yilbermolina from "./yilber-molina.jpg";
import zuleidisbarrios from "./zuleydis-barrios.jpg";
import saralopez from "./saralopez.jpg"

export const images = {
  "assets/img/alan-sanchez.jpg": alansanchez,
  "assets/img/alvaro-vergara.jpg": alvarovergara,
  "assets/img/amanda-quintana.jpg": amandaquintana,
  "assets/img/andres-molinares.jpg": andresmolinares,
  "assets/img/andres-garcia.jpg": andresgarcia,
  "assets/img/angelica-morales.jpg": angelicamorales,
  "assets/img/danith-babilonia.jpg": danithbabilonia,
  "assets/img/deiver-vazquez.jpg": deivervazquez,
  "assets/img/erick-contreras.jpg": erickcontreras,
  "assets/img/erick-herazo.jpg": erickherazo,
  "assets/img/ever-navarro.jpg": evernavarro,
  "assets/img/herly-castillo.jpg": herlycastillo,
  "assets/img/isaac-benavides.jpg": isaacbenavides,
  "assets/img/jair-calderon.jpg": jaircalderon,
  "assets/img/jhonatan-alvarez.jpg": jhonatanalvarez,
  "assets/img/jorge-zetien.jpg": jorgezetien,
  "assets/img/juan-payares.jpg": juanpayares,
  "assets/img/juan-lara.jpg": juanlara,
  "assets/img/juan-sierra.jpg": juansierra,
  "assets/img/leandro-meza.jpg": leandromeza,
  "assets/img/luis-tilve.jpg": luistilve,
  "assets/img/luis-payares.jpg": luispayares,
  "assets/img/maria-anaya.jpg": mariaanaya,
  "assets/img/mauricio-ariaspng.jpg": mauricioarias,
  "assets/img/oscar-ballestas.jpg": oscarballestas,
  "assets/img/ricardo-jaramillo.jpg": ricardojaramillo,
  "assets/img/ronald-paternina.jpg": ronaldpaternina,
  "assets/img/saralopez.jpg":saralopez,
  "assets/img/steeven-altamiranda.jpg": steevenaltamiranda,
  "assets/img/yilber-molina.jpg": yilbermolina,
  "assets/img/zuleydis-barrios.jpg": zuleidisbarrios  
};
