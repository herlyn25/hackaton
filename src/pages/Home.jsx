import image from '../assets/picture/team.png'
import banner from '../assets/picture/banner.png'

const bannerStyle={
    width:'100%',
    height:'100%'
}
const textStyle={
    fontSize:'2em',
    textAlign: 'justify',
    paddingLeft:'7em',
    marginTop:'1.5em'
    

}
const imgStyle ={
    height:'300px',
    width:'300px',
}

export function Home() {

    return(
        <div>
            <div >
                <img src={banner} alt="banner" style={bannerStyle} />
            </div>
            <h1 style={{
                padding:'2em 2em 1.5em 0',
                fontSize:'3.5em'
                }}>Generador de grupos aleatorios</h1>
            <div className='row my-5' >
                <div className='col-6'>
                <p style={textStyle}>
                    Este Aplicativo permite generar de forma aleatoria grupos o equipos de trabajo,
                    dado un archvo excel, una lista de elementos o un archivo Json
                </p>
                </div>
                <figure className='col-6'>    
                    <img src={image} alt="team" style={imgStyle} />
                </figure>
                
            </div>
        </div>
    );
}
