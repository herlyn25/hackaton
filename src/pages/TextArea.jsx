import { useState } from "react";
import { saveAs } from "file-saver";

export function TextArea() {
  const [text, setText] = useState("");

  const createFile = () => {
    let blob = new Blob([text], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "formato.json");
  };

  return (
    <div className="container py-5">
      <h1 className="py-2" value={text}>
        Cambiar a formato JSON{" "}
      </h1>
      <textarea
        className="form-control"
        id="exampleFormControlTextarea1"
        rows="14"
        placeholder="Ingrese el texto a convertir"
        value={text}
        onChange={(e) => setText(e.target.value)}
      ></textarea>
      <button onClick={createFile} className="btn btn-primary my-4 px-5 py-2">
        Cambiar
      </button>
    </div>
  );
}
